package com.mars.module.base;

import com.mars.common.util.RequestUtils;
import com.mars.common.util.TokenUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletRequest;

/**
 * 功能描述
 *
 * @author 程序员Mars
 * @version 1.0
 * @date 2023-10-28 17:37:23
 */
@Controller
@AllArgsConstructor
public  class BaseController {

    private final TokenUtils tokenUtils;


    /**
     * 获取用户ID
     *
     * @return Long
     */
    public synchronized Long getUserId() {
        HttpServletRequest request = RequestUtils.getRequest();
        return tokenUtils.getUserId(request);
    }

}
