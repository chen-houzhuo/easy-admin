package com.mars.module.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mars.module.admin.entity.Students;
import com.mars.framework.mapper.BasePlusMapper;

/**
 * 学生成绩Mapper接口
 *
 * @author mars
 * @date 2024-03-12
 */
public interface StudentsMapper extends BasePlusMapper<Students> {

}
