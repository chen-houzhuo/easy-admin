package com.mars.module.admin.mapper;

import com.mars.module.admin.entity.SysConfig;
import com.mars.framework.mapper.BasePlusMapper;

/**
 * 系统配置Mapper接口
 *
 * @author mars
 * @date 2023-11-20
 */
public interface SysConfigMapper extends BasePlusMapper<SysConfig> {

}
