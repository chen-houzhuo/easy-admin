package com.mars.module.admin.service;

import com.mars.module.admin.entity.SysMessage;
import com.mars.common.response.PageInfo;
import com.mars.module.admin.request.SysMessageRequest;

import java.util.List;

/**
 * 消息接口
 *
 * @author mars
 * @date 2023-12-06
 */
public interface ISysMessageService {
    /**
     * 新增
     *
     * @param param param
     * @return SysMessage
     */
    SysMessage add(SysMessageRequest param);

    /**
     * 删除
     *
     * @param id id
     * @return boolean
     */
    boolean delete(Long id);

    /**
     * 批量删除
     *
     * @param ids ids
     * @return boolean
     */
    boolean deleteBatch(List<Long> ids);

    /**
     * 更新
     *
     * @param param param
     * @return boolean
     */
    boolean update(SysMessageRequest param);

    /**
     * 查询单条数据，Specification模式
     *
     * @param id id
     * @return SysMessage
     */
    SysMessage getById(Long id);

    /**
     * 查询分页数据
     *
     * @param param param
     * @return PageInfo<SysMessage>
     */
    PageInfo<SysMessage> pageList(SysMessageRequest param);

    /**
     * 读取所有消息
     */
    void readAllMsg();

}
