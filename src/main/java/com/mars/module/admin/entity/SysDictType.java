package com.mars.module.admin.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.*;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import cn.afterturn.easypoi.excel.annotation.Excel;
import com.mars.module.system.entity.BaseEntity;

/**
 * 字典类型对象 sys_dict_type
 *
 * @author mars
 * @date 2023-11-18
 */

@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@ApiModel(value = "字典类型对象")
@Accessors(chain = true)
@TableName("sys_dict_type")
public class SysDictType extends BaseEntity {


    /**
     * ID
     */
    @TableId(value = "id" , type = IdType.AUTO)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "id")
    private Long id;

    /**
     * 名称
     */
    @Excel(name = "名称")
    @ApiModelProperty(value = "名称")
    private String name;

    /**
     * 类型
     */
    @Excel(name = "类型")
    @ApiModelProperty(value = "类型")
    private String type;

    /**
     * 状态 0启用 1 禁用
     */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private Integer state;
}
