package com.mars.module.system.mapper;


import com.mars.framework.mapper.BasePlusMapper;
import com.mars.module.system.entity.SysPost;


/**
 * 岗位
 *
 * @author 源码字节-程序员Mars
 */
public interface SysPostMapper extends BasePlusMapper<SysPost> {

}
