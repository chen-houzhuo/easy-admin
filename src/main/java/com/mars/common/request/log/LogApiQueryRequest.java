package com.mars.common.request.log;

import com.mars.common.request.PageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 接口日志查询DTO
 *
 * @author 源码字节-程序员Mars
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class LogApiQueryRequest extends PageRequest {

    @ApiModelProperty(value = "用户")
    private String module;

    @ApiModelProperty(value = "接口地址")
    private String url;

}
