package com.mars.common.request.sys;

import com.mars.common.request.PageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

/**
 * 功能描述
 *
 * @author 程序员Mars
 * @version 1.0
 * @date 2023-11-03 22:40:00
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SysPostRequest extends PageRequest {

    @ApiModelProperty(value = "ID")
    private Long id;

    @NotBlank(message = "请输入岗位名称")
    @ApiModelProperty(value = "岗位名称")
    private String postName;

    @NotBlank(message = "请输入岗位编码")
    @ApiModelProperty(value = "岗位编码")
    private String postCode;

    @ApiModelProperty(value = "备注")
    private String remark;

}
