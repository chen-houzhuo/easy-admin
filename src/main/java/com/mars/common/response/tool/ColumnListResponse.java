package com.mars.common.response.tool;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 数据库表列VO
 *
 * @author 源码字节-程序员Mars
 */
@Data
public class ColumnListResponse {

    @ApiModelProperty(value = "字段名")
    private String columnName;

    @ApiModelProperty(value = "备注")
    private String columnComment;

    @ApiModelProperty(value = "类型")
    private String columnType;

    @ApiModelProperty(value = "是否可为空")
    private String isNullable;

    @ApiModelProperty(value = "是否主键")
    private String columnKey;


}
